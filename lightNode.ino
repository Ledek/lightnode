/*
 * Inspired by:
 * SmartShow AirPixel ONE - Single Universe ArtNet to WS2812 Driver - For Wemos D1
 * www.smartshow.light
 */
/* 
 * TODO:
 * Controller priority
 *    similar to sequence, store highest priority only read messages with equal or higher prio
 *    reset to 0 after not reading any message within a set time
 * Multiple universes on single output
 * More universes
 * Multicast
 * ArtSync
 * Options
 * Remote adressing
 * Single universe multiple outputs
 */

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "config.h"
#include "pixelNode.h"  // Converts dmx channel data to bit streams.
#include "netNode.h"    // Handles incoming traffic and parses artnet/sacn packets.

/************** FUNCTION PROTOTYPES *************/
bool netNode();
bool setup_netNode();
bool pixelNode(uint8_t*);
bool setup_pixelNode();

void setup() {
  setup_pixelNode();
  
  /* Turn on status led "Connecting" */
  rgbbanger(0x00FF00);
  
  Serial.begin(115200);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  /* Connect to network */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
//  WiFi.config(local_ip, gateway_ip, subnet_ip);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.print("Connection failed: ");
    switch(WiFi.status()) {
      case WL_NO_SSID_AVAIL : 
        rgbbanger(0xFF0000);
        Serial.println("SSID cannot be reached");
        break;
      case WL_CONNECT_FAILED : 
        rgbbanger(0xFF66000);
        Serial.println("Incorrect password");
        break;
      default :
        rgbbanger(0x555555);
        Serial.println("Unknown error");
    }

    Serial.println("Rebooting...");
    delay(5000);
    ESP.restart();
  }
  
  /* Open ethernet ports */
  udp_artnet.begin(artNet_port);
  udp_sacn.begin(sACN_port);
  
  /* Status "Connected" */
  rgbbanger(0x000066);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("Universe: ");
  Serial.println(WiFi.localIP()[3]);
  
  /* Setup OTA */
  ArduinoOTA.setPort(ota_port);     // Port defaults to 8266
//  ArduinoOTA.setHostname(ota_host); // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setPassword(ota_pass); // No authentication by default
  ArduinoOTA.onStart([]() { 
    delay(10);
    rgbbanger(0x00FF00);
  });
  ArduinoOTA.onEnd([]() { 
    delay(10);
    rgbbanger(0x0000FF);
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) { 
    delay(10);
    rgbbanger(0xFFFF00);
  });
  ArduinoOTA.onError([](ota_error_t error) {
    delay(10);
    rgbbanger(0xFF0000);
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("OTA Ready");
}

void loop() {
  ArduinoOTA.handle();
  netNode();
}
/*
// Timer
unsigned long timer, timer2, timer3, timer4;
timer3 = micros();
timer4 = micros();
Serial.println(timer4 - timer3);
*/
