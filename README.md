# LightNode #
-------------
__Version 1.7.4__

1. Features
2. Configuration
3. Status codes
4. Version history

### 1. FEATURES ###
* Art-Net
* sACN
* RGB -> RGBW (broken)
* Control strip as one pixel
* Discards out-of-order packets (on sACN)
* 2 universes on separate outputs
* OTA updates

### 2. CONFIGURATION ###
. . .

### 3. STATUS CODES ###
#### Power on ####
COLOR   |   STATUS
-----   |   ------
Green   |   Connecting
Blue    |   Connected
Red     |   Cannot find SSID
Yellow  |   Incorrect password
Purple  |   Unknown error

#### Firmware update ####
COLOR   |   STATUS
-----   |   ------
Green   |   Connecting
Blue    |   Connected
Red     |   Cannot find SSID
Yellow  |   Incorrect password

### 4. VERSION HISTORY ###
* 1.7.4    First.
* 1.7.2    Refactoring, fixed flashing green pixel.
* 1.7      Added OTA update support, reconfigured status leds, refactoring.
* 1.6      Added simple status LED.
* 1.5      Added dual output from two universes.
