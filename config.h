/******************* SETTINGS *******************/
/* ENABLE FEATURES */
const bool        enable_onePixel   = false;              // Treat each output as one pixel each
//const bool        enable_cloneOut   = false;              // Clone all outputs
/* GENERAL */
const uint8_t     colors_input      = 3;                  // Colors per pixel in controller
//const uint8_t     colors_output     = 3;                  // Colors per physical pixel
//const uint8_t     output            = 2;                  // 1-2 // Number of outputs
//const uint8_t     output_unis       = 1;                  // 1-2 // Universes per output
/* NETWORK */
const uint16_t    artNet_port       = 6454;               // Network port for Art-Net // Default: 6454
const uint16_t    sACN_port         = 5568;               // Network port for sACN // Default: 5568
const char*       ssid              = "";
const char*       password          = "";
/* OTA UPDATES */
const uint16_t    ota_port          = 8266;               // Port for OTA updates // Default: 8266
const char*       ota_pass          = ""; // No authentication by default
