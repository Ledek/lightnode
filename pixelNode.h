/************** CONSTANT PARAMETERS *************/
/* ESP8266 */
#define OUTPUT1_PIN 5 // Pin D1
#define OUTPUT2_PIN 4 // Pin D2

/************** FUNCTION PROTOTYPES *************/
void bitbanger(uint8_t);
void rgbbanger(uint32_t);

uint32_t outputBit = (1<<OUTPUT1_PIN);

bool setup_pixelNode() {
  pinMode(OUTPUT1_PIN, OUTPUT);
  pinMode(OUTPUT2_PIN, OUTPUT);
}

bool ICACHE_FLASH_ATTR pixelNode(uint8_t *uniData, uint16_t &uniSize) {
  uint16_t chan;
  os_intr_lock();
  for (uint16_t t = 0; t < uniSize; t++) {  // outer loop counting bytes
    chan = enable_onePixel ? (t % colors_input) : t;
    bitbanger(uniData[chan]); // ~10us
//    if ((t % 3) == 2) bitbanger(0x00); // TODO: Generalize for mor than 3->4
  }
  os_intr_unlock();
}

void ICACHE_RAM_ATTR bitbanger(uint8_t chData) {
  uint8_t n;
  uint8_t bitMask = 0x80;
  while (bitMask) {
    // T = 0ns
    n = 4;
    while (n--) WRITE_PERI_REG( 0x60000304, outputBit );  // 75ns @ 160MHz
    // T = 300ns @ 160MHz
    n = 4;
    if (chData & bitMask)
      while (n--) WRITE_PERI_REG( 0x60000308, 0 );  // 75ns @ 160MHz
    else
      while (n--) WRITE_PERI_REG( 0x60000308, outputBit );  // 75ns @ 160MHz
    // T = 900ns @ 160MHz
    n = 8;
    while (n--) WRITE_PERI_REG( 0x60000308, outputBit );  // 75ns @ 160MHz
    // T = 1200ns @ 160MHz
    bitMask >>= 1;
  }
}

void ICACHE_RAM_ATTR rgbbanger(uint32_t data) {
  bitbanger((data >> 8) & 0xFF);
  bitbanger((data >> 16) & 0xFF);
  bitbanger(data & 0xFF);
//  bitbanger(0x00);
}
